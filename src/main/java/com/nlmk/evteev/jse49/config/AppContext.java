package com.nlmk.evteev.jse49.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.nlmk.evteev.jse49")
public class AppContext {
}
