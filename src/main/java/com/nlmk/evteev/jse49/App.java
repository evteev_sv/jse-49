package com.nlmk.evteev.jse49;

import com.nlmk.evteev.jse49.config.AppContext;
import com.nlmk.evteev.jse49.service.ApplicationProcess;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Log4j2
public class App {
    public static void main(String[] args) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(AppContext.class);
        ApplicationProcess applicationProcess = (ApplicationProcess) ctx.getBean(ApplicationProcess.class);
        try {
            applicationProcess.process();
        } catch (RuntimeException re) {
            log.error(re.getMessage());
        }
    }

}
