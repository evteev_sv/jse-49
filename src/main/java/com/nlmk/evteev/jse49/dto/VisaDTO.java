package com.nlmk.evteev.jse49.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VisaDTO {

    private String name;
    private String firstname;
    private String secname;
    private String nationality;

    private int age;

}
