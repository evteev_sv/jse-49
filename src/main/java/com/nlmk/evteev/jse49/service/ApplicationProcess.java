package com.nlmk.evteev.jse49.service;

import com.nlmk.evteev.jse49.constant.TerminalConstant;
import com.nlmk.evteev.jse49.interfaces.ICommandInterface;
import com.nlmk.evteev.jse49.interfaces.IFormDataSender;
import com.nlmk.evteev.jse49.interfaces.IVisaApplicationFormInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ApplicationProcess {

    private ICommandInterface commandInterface;
    private IFormDataSender formDataSender;
    private String visaData = "";

    private IVisaApplicationFormInterface russianVisa;
    private IVisaApplicationFormInterface japanVisa;

    @Autowired
    public ApplicationProcess(ICommandInterface commandInterface, IFormDataSender formDataSender,
                              @Qualifier("russianVisaImpl") IVisaApplicationFormInterface russianVisa,
                              @Qualifier("japanVisaImpl") IVisaApplicationFormInterface japanVisa) {
        this.commandInterface = commandInterface;
        this.formDataSender = formDataSender;
        this.russianVisa = russianVisa;
        this.japanVisa = japanVisa;
    }

    public void process() {
        String cmd = "";
        while (!TerminalConstant.CMD_EXIT.equals(cmd)) {
            cmd = commandInterface.writeAndRead("Наберите команду help для вывода всех команд.");
            switch (cmd) {
                case TerminalConstant.CMD_RUSSIAN_VISA:
                    russianVisa.fillForm();
                    visaData = russianVisa.getFormData();
                    break;
                case TerminalConstant.CMD_JAPAN_VISA:
                    japanVisa.fillForm();
                    visaData = japanVisa.getFormData();
                    break;
                case TerminalConstant.CMD_HELP:
                    StringBuilder builder = new StringBuilder();
                    builder.append("Команды консоли:\n")
                            .append("russian-visa - формирование российской визы\n")
                            .append("japan-visa - формирование японской визы\n")
                            .append("visa-print - вывод данных по визе\n")
                            .append("exit - завершение работы\n")
                            .append("help - вывод доступных комманд\n");
                    System.out.println(builder.toString());
                    break;
                case TerminalConstant.CMD_PRINT_VISA:
                    if (!visaData.isEmpty()) {
                        formDataSender.send(visaData);
                        visaData = "";
                    } else {
                        System.out.println("Данных для формирования визы не найдено. Сформируйте сначала данные.");
                    }
                    break;
                case TerminalConstant.CMD_EXIT:
                    return;
                default:
                    commandInterface.write("Неверный выбор или команда.\nНаберите команду help для вывода всех команд.");
                    break;
            }
        }
    }
}
