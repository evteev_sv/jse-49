package com.nlmk.evteev.jse49.interfaces;

public interface IVisaApplicationFormInterface {

    /**
     * Запрашивает и заполняет данные формы.
     */
    void fillForm();

    /**
     * Возвращает данные формы.
     */
    String getFormData();

}
