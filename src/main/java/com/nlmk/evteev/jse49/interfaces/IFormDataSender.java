package com.nlmk.evteev.jse49.interfaces;

public interface IFormDataSender {

    /**
     * Отправляет данные формы.
     *
     * @param data данные формы.
     */
    void send(String data);
}
