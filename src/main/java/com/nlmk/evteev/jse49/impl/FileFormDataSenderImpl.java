package com.nlmk.evteev.jse49.impl;

import com.nlmk.evteev.jse49.interfaces.IFormDataSender;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
@Log4j2
public class FileFormDataSenderImpl implements IFormDataSender {

    /**
     * Отправляет данные формы.
     *
     * @param data данные формы.
     */
    @Override
    public void send(String data) {
        File file = new File("data/", "visa.txt");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(data);
            fileWriter.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

}
