package com.nlmk.evteev.jse49.impl;

import com.nlmk.evteev.jse49.interfaces.IFormDataSender;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class ConsoleFormDataSenderImpl implements IFormDataSender {

    /**
     * Отправляет данные формы.
     *
     * @param data данные формы.
     */
    @Override
    public void send(String data) {
        System.out.println(data);
    }
}
