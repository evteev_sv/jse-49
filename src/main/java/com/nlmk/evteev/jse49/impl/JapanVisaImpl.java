package com.nlmk.evteev.jse49.impl;

import com.nlmk.evteev.jse49.dto.VisaDTO;
import com.nlmk.evteev.jse49.interfaces.ICommandInterface;
import com.nlmk.evteev.jse49.interfaces.IVisaApplicationFormInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JapanVisaImpl implements IVisaApplicationFormInterface {

    private ICommandInterface commandInterface;
    private VisaDTO dto;

    @Autowired
    public void setCommandInterface(ICommandInterface commandInterface) {
        this.commandInterface = commandInterface;
    }

    /**
     * Запрашивает и заполняет данные формы.
     */
    @Override
    public void fillForm() {
        dto = VisaDTO.builder()
                .name(commandInterface.writeAndRead("Введите имя: "))
                .firstname(commandInterface.writeAndRead("Введите фамилию: "))
                .secname(commandInterface.writeAndRead("Введите отчество: "))
                .age(Integer.parseInt(commandInterface.writeAndRead("Введите возраст: ")))
                .nationality(commandInterface.writeAndRead("Гражданство: "))
                .build();

    }

    /**
     * Возвращает данные формы.
     */
    @Override
    public String getFormData() {
        StringBuilder builder = new StringBuilder();
        builder.append("Данные для японской визы:\n")
                .append("Фамилия: ")
                .append(dto.getFirstname().concat("\n"))
                .append("Имя: ")
                .append(dto.getName().concat("\n"))
                .append("Отчество: ")
                .append(dto.getSecname().concat("\n"))
                .append("Возраст: ")
                .append(dto.getAge() + "\n")
                .append("Гражданство: ")
                .append(dto.getNationality().concat("\n"));
        return builder.toString();
    }
}
