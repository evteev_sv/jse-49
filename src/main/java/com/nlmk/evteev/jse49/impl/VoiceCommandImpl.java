package com.nlmk.evteev.jse49.impl;

import com.nlmk.evteev.jse49.interfaces.ICommandInterface;
import org.springframework.stereotype.Component;

@Component
public class VoiceCommandImpl implements ICommandInterface {

    @Override
    public String writeAndRead(String text) {
        throw new UnsupportedOperationException("Operation not supported!");
    }

    @Override
    public void write(String text) {
        throw new UnsupportedOperationException("Operation not supported!");
    }
}
