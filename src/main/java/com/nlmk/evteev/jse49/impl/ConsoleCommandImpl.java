package com.nlmk.evteev.jse49.impl;

import com.nlmk.evteev.jse49.constant.TerminalConstant;
import com.nlmk.evteev.jse49.interfaces.ICommandInterface;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Primary
@Component
@Scope("prototype")
public class ConsoleCommandImpl implements ICommandInterface {

    /**
     * Отправить и получить данные из интерфейса.
     *
     * @param text данные для отправки в интерфейс.
     * @return данные полученные от интерфейса.
     */
    @Override
    public String writeAndRead(String text) {
        System.out.println(text);
        return TerminalConstant.scanner.nextLine();
    }

    /**
     * Отправить данные в интерфейс.
     *
     * @param text данные для отправки в интерфейс.
     */
    @Override
    public void write(String text) {
        System.out.println(text);
    }
}
