package com.nlmk.evteev.jse49.constant;

import java.util.Scanner;

/**
 * Класс с терминальными константами
 */
public class TerminalConstant {

    public static final String CMD_EXIT = "exit";
    public static final String CMD_HELP = "help";
    public static final String CMD_RUSSIAN_VISA = "russian-visa";
    public static final String CMD_JAPAN_VISA = "japan-visa";
    public static final String CMD_PRINT_VISA = "visa-print";
    public static Scanner scanner = new Scanner(System.in);

}
